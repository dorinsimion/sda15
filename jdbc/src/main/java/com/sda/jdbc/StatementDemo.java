package com.sda.jdbc;

import java.sql.*;
import java.util.List;
import java.util.Set;

public class StatementDemo {

    private StatementDemo(){}

    public static void addStudent(String nume){
        String sql = "INSERT INTO student (nume)"+
                " VALUES ('"+nume+"')";
//        Class.forName("com.mysql.jdbc.Driver");
        try(Connection conn=DbConnection.getConnection();
            Statement stmt = conn.createStatement()
        ) {
            int count = stmt.executeUpdate(sql);
            if(count==1){
                System.out.println("Am adaugat un student!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addProfesor(String nume){
        String sql = "INSERT INTO profesor (nume)"+
                " VALUES ('"+nume+"')";
//        Class.forName("com.mysql.jdbc.Driver");
        try(Connection conn=DbConnection.getConnection();
            Statement stmt = conn.createStatement()
        ) {
            int count = stmt.executeUpdate(sql);
            if(count==1){
                System.out.println("Am adaugat un profesor!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteAllStudents(){
        String sql = "DELETE FROM student";
        try(Connection conn = DbConnection.getConnectionWithDataSource();
            Statement stmt = conn.createStatement()){
            int deleteRows = stmt.executeUpdate(sql);
            System.out.println("Au fost stersi "+deleteRows+
                    " studenti din tabela student!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateStudentName(String oldName,String newName){
//        String sql = "UPDATE student SET nume='"+
//                newName+"' WHERE nume='"+oldName+"'";
        StringBuilder sb = new StringBuilder("UPDATE student SET nume='")
                .append(newName)
                .append("' WHERE nume='")
                .append(oldName)
                .append("'");
        System.out.println(sb.toString());
        try(Connection conn = DbConnection.getConnection();
            Statement stmt = conn.createStatement()) {
            int updatedStudents = stmt.executeUpdate(sb.toString());
            System.out.println("Am modificat "+updatedStudents+
                    " studenti!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void displayAllStudents(){
        String sql = "SELECT * FROM student";
        try(Connection conn= DbConnection.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet resultSet =stmt.executeQuery(sql)) {
            while(resultSet.next()) {
                System.out.println(resultSet.getInt(1) + " : " +
                        resultSet.getString("nume"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void searchTeacherByName(String name){
        String sql = "SELECT nume FROM profesor "+
                "WHERE nume='"+name+"'";
        try(Connection conn = DbConnection.getConnection();
        Statement stmt = conn.createStatement()) {
            boolean isSELECT = stmt.execute(sql);
            if(isSELECT){
                System.out.println("Lista profesori cu numele "+name);
                try(ResultSet resultSet = stmt.getResultSet()){
                    while(resultSet.next()){
                        System.out.println(resultSet.getString(1));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addCourses(Set<String> courseList){
        String sql = "INSERT INTO materie (nume) VALUES";
        for(String course: courseList){
            sql+="('"+course+"'),";
        }
        sql = sql.substring(0,sql.length()-1);
       try(Connection conn = DbConnection.getConnection();
       Statement stmt = conn.createStatement()){
           boolean isSELECT = stmt.execute(sql);
           if(!isSELECT){
               int updateCount = stmt.getUpdateCount();
               System.out.println("Am introdus "
                       +updateCount+" cursuri!");
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }
    }
}
