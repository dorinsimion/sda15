package com.sda.jdbc;

import java.sql.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class JdbcDemo {
    public static void main(String[] args) throws SQLException {
        try (Connection conn = DbConnection.getConnection()) {
            System.out.println(conn);
        }
        Connection conn2=null;
        // old way, use try-with-resources
        try  {
            conn2 = DbConnection.getConnectionWithDataSource();
            System.out.println(conn2);
        } finally {
            if(conn2!=null)
                conn2.close();
        }
        StatementDemo.deleteAllStudents();
        StatementDemo.addStudent("Ion");
        StatementDemo.addStudent("Mihai");
        StatementDemo.updateStudentName("Ion","George");

        StatementDemo.addProfesor("Teacher1");
        StatementDemo.addProfesor("Teacher2");
        StatementDemo.displayAllStudents();
        StatementDemo.searchTeacherByName("Teacher1");

        Set<String> courses = new HashSet<>();
        courses.add("Romana");
        courses.add("Matematica");
        courses.add("Istorie");
        StatementDemo.addCourses(courses);

        PreparedStatementDemo.addNota("Mihai","Romana",10);
        PreparedStatementDemo.addTeacher("Teacher3");
        TransactionDemo.addStudents(Arrays.asList("Igor","Silviu"));
        TransactionDemo.addBatchStudents(Arrays.asList("Igor","Silviu"));
    }
}
