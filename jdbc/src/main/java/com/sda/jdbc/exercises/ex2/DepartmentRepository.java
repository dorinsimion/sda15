package com.sda.jdbc.exercises.ex2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

class DepartmentRepository {
    private static final DepartmentRepository INSTACE =
            new DepartmentRepository();

    private DepartmentRepository(){}

    static DepartmentRepository getInstace(){
        return INSTACE;
    }

    List<Department> findAll(){
        String sql = "SELECT * FROM departments";
        List<Department> departments = new ArrayList<>();
        try(Connection conn = DbConnection.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet resultSet = stmt.executeQuery()){
            while(resultSet.next()){
                Department d = new Department();
                d.setId(resultSet.getInt(1));
                d.setName(resultSet.getString(2));
                departments.add(d);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return departments;
    }

    Optional<Department> findById(int id){
        String sql = "SELECT * FROM departments "+
                "WHERE departmentid = ?";
        try(Connection conn = DbConnection.getConnection();
            PreparedStatement stmt= conn.prepareStatement(sql)){
            stmt.setInt(1,id);
            try(ResultSet resultSet = stmt.executeQuery()) {
                if (resultSet.next()) {
                    Department d = new Department();
                    d.setId(resultSet.getInt(1));
                    d.setName(resultSet.getString(2));
                    return Optional.of(d);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    int deleteById(int id){
        String sql ="DELETE FROM employees WHERE departmentid = ?";
        try(Connection conn = DbConnection.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql)){
            stmt.setInt(1,id);
            return stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    boolean save(Department department){
        String sql = "INSERT INTO departments VALUES(NULL,?)";
        try(Connection conn= DbConnection.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql)){
            stmt.setString(1,department.getName());
            return stmt.executeUpdate()==1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    boolean update(Department d){
        String sql = "UPDATE departments SET name =? "+
                "WHERE departmentId=?";
        try(Connection conn = DbConnection.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql)){
            stmt.setString(1,d.getName());
            stmt.setInt(2,d.getId());
            return stmt.executeUpdate()!=0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    List<Department> findByName(String nume){
        String sql = "SELECT * FROM departments WHERE name=?";
        List<Department> departments = new ArrayList<>();
        try(Connection conn = DbConnection.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql)){
            stmt.setString(1,nume);
            try(ResultSet resultSet = stmt.executeQuery()){
                while(resultSet.next()){
                    Department d = new Department();
                    d.setId(resultSet.getInt(1));
                    d.setName(resultSet.getString(2));
                    departments.add(d);
                }}
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return departments;
    }

}
