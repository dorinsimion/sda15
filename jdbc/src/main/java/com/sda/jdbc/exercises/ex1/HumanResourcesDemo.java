package com.sda.jdbc.exercises.ex1;

import java.util.List;

public class HumanResourcesDemo {
    public static void main(String[] args) {
        HumanResourcesRepository repo = new HumanResourcesRepository();
        List<Project> projects = repo.getProjects();
        System.out.println("Project list");
        projects.forEach(System.out::println);
        System.out.println("**************");
        System.out.println("Employee list");
        List<Employee> employees = repo.getEmployees();
        employees.forEach(System.out::println);
        System.out.println("**************");
        System.out.println("Employee list with J");
        employees = repo.getEmployeesNameStartWithJ();
        employees.forEach(System.out::println);
        System.out.println("**************");
        System.out.println("Employee list with no department");
        employees = repo.getEmployeesWithNoDeparment();
        employees.forEach(System.out::println);
        System.out.println("**************");
        System.out.println("Employee list with department");
        System.out.println("| Id |   First Name |    Last Name |   Birthday |   Department|");
        System.out.println("---------------------------------------------------------------");
        employees = repo.getEmployeesWithDepartment();
        employees.forEach(e -> System.out.println(e.print()));
        System.out.println("---------------------------------------------------------------");
    }
}
