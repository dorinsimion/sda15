package com.sda.jdbc.exercises.ex2;

public class DemoEx2 {
    public static void main(String[] args) {
        DepartmentRepository repo = DepartmentRepository.getInstace();
        System.out.println("Departments");
        repo.findAll().forEach(System.out::println);
        System.out.println("Department with id 1");
        repo.findById(1).ifPresent(System.out::println);
        System.out.println("Department with id 100");
        try {
            repo.findById(100).orElseThrow(() -> new RuntimeException("Not exists!"));
        } catch(RuntimeException e){
            System.out.println("Err");
        }

        System.out.println("Deleted users from department 2");
//        repo.deleteById(2);
        Department d = new Department();
        d.setName("IT");
        System.out.println("Successful add? "+ repo.save(d));
        d = new Department();
        d.setId(1);
        d.setName("Marketing");
        repo.update(d);
        repo.findAll().forEach(System.out::println);
        System.out.println("Find IT departments");
        repo.findByName("IT").forEach(System.out::println);
    }
}
