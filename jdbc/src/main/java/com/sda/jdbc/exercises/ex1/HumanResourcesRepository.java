package com.sda.jdbc.exercises.ex1;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

class HumanResourcesRepository {
    final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    List<Project> getProjects(){
        String sql = "SELECT * FROM projects";
        List<Project> projectList = new ArrayList<>();
        try(Connection conn= DbConnection.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet resultSet =stmt.executeQuery(sql)) {
            while(resultSet.next()) {
                Project p = new Project();
                p.setId(resultSet.getInt(1));
                p.setDescription(resultSet.getString(2));
                projectList.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return projectList;
    }

    List<Employee> getEmployees(){
        String sql = "SELECT employeeId,firstname,lastname,dateofbirth FROM employees";
        List<Employee> employeeList = new ArrayList<>();
        try(Connection conn= DbConnection.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet resultSet =stmt.executeQuery(sql)) {
            while(resultSet.next()) {
                Employee e = new Employee();
                e.setId(resultSet.getInt(1));
                e.setFirstName(resultSet.getString(2));
                e.setLastName(resultSet.getString(3));
                e.setDateOfBirthday(LocalDate.parse(resultSet.getString(4),dtf));
                employeeList.add(e);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return employeeList;
    }

    List<Employee> getEmployeesNameStartWithJ(){
        String sql = "SELECT employeeId,firstname,lastname,dateofbirth FROM employees where firstname like 'J%' OR lastname like 'J%'";
        List<Employee> employeeList = new ArrayList<>();
        try(Connection conn= DbConnection.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet resultSet =stmt.executeQuery(sql)) {
            while(resultSet.next()) {
                Employee e = new Employee();
                e.setId(resultSet.getInt(1));
                e.setFirstName(resultSet.getString(2));
                e.setLastName(resultSet.getString(3));
                e.setDateOfBirthday(LocalDate.parse(resultSet.getString(4),dtf));
                employeeList.add(e);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return employeeList;
    }

    List<Employee> getEmployeesWithNoDeparment(){
        String sql = "SELECT employeeId,firstname,lastname,dateofbirth FROM employees where departmentid is null";
        List<Employee> employeeList = new ArrayList<>();
        try(Connection conn= DbConnection.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet resultSet =stmt.executeQuery(sql)) {
            while(resultSet.next()) {
                Employee e = new Employee();
                e.setId(resultSet.getInt(1));
                e.setFirstName(resultSet.getString(2));
                e.setLastName(resultSet.getString(3));
                e.setDateOfBirthday(LocalDate.parse(resultSet.getString(4),dtf));
                employeeList.add(e);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return employeeList;
    }

    List<Employee> getEmployeesWithDepartment(){
        String sql = "SELECT e.employeeId,e.firstname,e.lastname,e.dateofbirth,d.departmentid,d.name "+
                "FROM employees e INNER JOIN departments d on e.departmentid=d.departmentid";
        List<Employee> employeeList = new ArrayList<>();
        try(Connection conn= DbConnection.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet resultSet =stmt.executeQuery(sql)) {
            while(resultSet.next()) {
                Employee e = new Employee();
                e.setId(resultSet.getInt(1));
                e.setFirstName(resultSet.getString(2));
                e.setLastName(resultSet.getString(3));
                e.setDateOfBirthday(LocalDate.parse(resultSet.getString(4),dtf));
                Department d = new Department();
                d.setId(resultSet.getInt(5));
                d.setName(resultSet.getString(6));
                e.setDepartment(d);
                employeeList.add(e);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return employeeList;
    }
}
