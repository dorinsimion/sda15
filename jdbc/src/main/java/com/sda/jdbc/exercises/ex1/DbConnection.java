package com.sda.jdbc.exercises.ex1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {
    private static final String DB_URL="jdbc:mysql://localhost:3306/human_resources_Exercise";
    private static final String USERNAME ="root";
    private static final String PASSWORD ="admin123";
    private DbConnection(){}

    public static Connection getConnection() throws SQLException {
        Connection conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
        return conn;
    }
}
