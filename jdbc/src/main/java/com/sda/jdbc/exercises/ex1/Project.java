package com.sda.jdbc.exercises.ex1;

public class Project {
    private int id;
    private String description;

    Project(){}

    int getId() {
        return id;
    }

    void setId(int id) {
        this.id = id;
    }

    String getDescription() {
        return description;
    }

    void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return id+":"+description;
    }
}
