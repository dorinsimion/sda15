package com.sda.jdbc;

import java.sql.*;

public class SqlInjection {
    public static void main(String[] args) {
        PreparedStatementDemo.createUsersTable();
        badLogin("Ion","123456");
        badLogin("Ion","1234");
        badLogin("Ion",
                "' or password=(SELECT password from users where username='Ion') or password='");
        System.out.println("*******");
        login("Ion","123456");
        login("Ion","1234");
        login("Ion",
                "' or password=(SELECT password from users where username='Ion') or password='");

    }

    public static void badLogin(String username, String password){
        String sql = "SELECT username FROM users WHERE "+
                "username='"+username+"' AND password='"+
                password+"'";
        try(Connection conn = DbConnection.getH2Connection();
            Statement stmt = conn.createStatement();
            ResultSet resultSet= stmt.executeQuery(sql)
        ){
//            System.out.println(conn);
//            System.out.println(stmt);
//            System.out.println(resultSet);
            if(resultSet.next()){
                System.out.println("Login");
            }else {
                System.out.println("Wrong username or password!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void login(String username, String password){
        String sql ="SELECT username FROM users where username=?"+
                " and password=?";
        try(Connection conn= DbConnection.getH2Connection();
            PreparedStatement stmt = conn.prepareStatement(sql)){
            stmt.setString(1,username);
            stmt.setString(2,password);
            try(ResultSet resultSet=stmt.executeQuery()){
                if(resultSet.next()){
                    System.out.println("Login");
                } else {
                    System.out.println("Wrong username or password!");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
