package com.sda.generics;

// can hold only toy cars
public class RegularBox {

    private ToyCar toyCar;

    // set by constructor
    public RegularBox(ToyCar toyCar) {
        this.toyCar = toyCar;
    }
}
