package com.sda.functional.lambda;

import java.util.*;
import java.util.function.*;

public class FunctionalInterfaces {

    public static void main(String[] args) {
        Predicate<String> test1 = (str) -> !str.isEmpty();
        Predicate<Integer> test2 =(i) -> i==0;
        Predicate<String> test3 =(str) -> str.startsWith("B");
        Predicate<String> test4 = test1.and(test3);
        Predicate<String> test5 = (s)-> !s.isEmpty() && s.startsWith("B");

        BiPredicate<String,String> test6 = (s1,s2) -> s1.startsWith(s2);
        BiPredicate<String,Integer> test7 =(s1,s2) -> s1.length()==s2;

        System.out.println(test7.test("ana",3));

        Supplier<Integer> supplier = () -> new Random().nextInt(6);
        System.out.println(supplier.get());
        Supplier<List<String>> listSupplier = () -> new ArrayList<>();
        List<String> strings = listSupplier.get();

        Consumer<String> consumer= (s)->System.out.println(s);
        consumer.accept("Mihai");
        BiConsumer<String,Integer> consumer1= (s,i)-> System.out.println(s+":"+i);
        consumer1.accept("Ion",3);
        List<String> list = listSupplier.get();
        Consumer<String> addToList = e -> list.add(e);
        addToList.accept("Ion");

        Function<String,Integer> calcLength = s4 -> s4.length();
        System.out.println(calcLength.apply("Mihai"));

        BiFunction<String,String,String> concat = (s1,s2)-> s1+s2;
        System.out.println(concat.apply("ion","jhon"));

        UnaryOperator<String> result = (s) -> s.substring(1);
        BinaryOperator<String> concatUnary = (s1,s2)-> s1+s2;
        System.out.println(concatUnary.apply("ion","mihai"));

        IntFunction<String> intFunction = (integer) -> ""+integer;
        IntSupplier intSupplier =()->1;
        System.out.println(intSupplier.getAsInt());

        Supplier<Set<String>> set = () -> new HashSet<String>();
        BiPredicate<Integer,Integer> check = (i1,i2) -> i1>i2;
        BiFunction<Integer,Integer,Boolean> check2= (i1,i2) -> i1>i2;
        BinaryOperator<Integer> produs = (i1,i2)-> i1*i2;
        Concantate<String,Integer> concantate = (s1,s2,s3)->s1.concat(s2).concat(s3).length();
        System.out.println((concantate.concat("a","b","c")));
    }
}
