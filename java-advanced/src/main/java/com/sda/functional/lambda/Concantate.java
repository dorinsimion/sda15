package com.sda.functional.lambda;

public interface Concantate<T,R> {
    R concat(T s1, T s2, T s3);
}
