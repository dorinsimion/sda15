package com.sda.functional.lambda;

public class TestStringContains implements Test {
    @Override
    public boolean test(String str) {
        return str.contains("A");
    }
}
