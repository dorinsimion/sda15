package com.sda.functional.lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LambdaWithCollections {
    public static void main(String[] args) {
        Comparator<Student> comparator =(s1,s2)-> s1.getNota().compareTo(s2.getNota());
        List<Student> list = new ArrayList<>();
        Collections.sort(list,(s1,s2)-> s1.getNota().compareTo(s2.getNota()));

        List<String> strings = new ArrayList<>();
        strings.add("ABC");
        strings.add("Abc");
        strings.add("qad");
        strings.removeIf(s-> s.startsWith("A"));
        System.out.println(strings);
        strings.add("ABC");
        strings.add("Abc");
        List<String> list2= new ArrayList<>();
        strings.forEach(s1->list2.add(s1.substring(1)));
        System.out.println(list2);
        strings.forEach(System.out::println);
    }
}
