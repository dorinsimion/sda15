package com.sda.functional.lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class MethodRefferenceDemo {
    public static void main(String[] args) {
        //with constructor
        Supplier<List<Integer>> supplierLambda = ()-> new ArrayList<>();
        Supplier<List<Integer>> supplier = ArrayList::new;
        //instace
        Predicate<String> instanceLambda = (s)-> s.isEmpty();
        Predicate<String> instance = String::isEmpty;

        //parameter
        List<Integer> list =supplier.get();
        Consumer<Integer> consumer = (i) -> list.add(i);
        Consumer<Integer> consumerWithMethodRef = list::add;
        Consumer<String> print = System.out::println;

        BiPredicate<String,String> startWith = (s1,s2)->s1.startsWith(s2);
        BiPredicate<String,String> startWith2 = String::startsWith;

        //nu merge method refference
        Predicate<String> p = (s)->s.startsWith("A");


        //alte sintaxe lambda
        //fara paranteze cand ai un singur parametru
        Predicate<String> p1 = s->s.startsWith("A");
        //vrei sa fii mai explicit
        p1 = (String s)->s.startsWith("A");
        //cand ai multe instructiuni de executat
        p1 = s->{ s=s+"abc";
            return s.startsWith("A");};
    }
}
