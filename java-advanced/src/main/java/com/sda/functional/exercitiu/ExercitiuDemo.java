package com.sda.functional.exercitiu;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class  ExercitiuDemo{
    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<>();
        Student s1 = new Student("Ion",4);
        studentList.add(s1);
        studentList.add(new Student("Mihai",6));
        studentList.add(new Student("George",7));
        studentList.add(new Student("Ion",2));
        studentList.add(new Student("Vali",10));

        List<Student> listMoreThan5 = studentList.stream()
                .filter(x -> x.getNota() >= 5)
                .collect(Collectors.toList());
        System.out.println(listMoreThan5);
        boolean isAnyWith10 = studentList.stream()
                .anyMatch(x -> x.getNota() == 0);
        System.out.println("Exista student cu 10? "+isAnyWith10);

//        List<Student> newList = studentList.stream()
//                .map(x -> {
//                    x.setNota(x.getNota() - 1);
//                    return x;
//                })
//                .sorted((x1,x2)->x1.getNota()-x2.getNota())
//                .collect(Collectors.toList());
//        System.out.println(newList);
//        List<Student> newList2 = studentList.stream()
//                .peek(x-> x.setNota(x.getNota()))
//                .sorted((x1,x2)->x1.getNota()-x2.getNota())
//                .collect(Collectors.toList());
//        System.out.println(newList2);

        List<Student> newList3 = studentList.stream()
                .map(x -> new Student(x.getNume(),x.getNota()-1))
                .sorted((x1,x2)->x1.getNota()-x2.getNota())
                .collect(Collectors.toList());
        System.out.println(newList3);
        studentList.stream()
                .filter(x->x.getNota()>=6&&x.getNota()<=8)
                .forEach(System.out::println);
        boolean allMoreThan5 = studentList.stream()
                .allMatch(x -> x.getNota() >= 5);
        System.out.println(allMoreThan5);
        Map<Integer, List<Student>> map = studentList.stream()
                .collect(Collectors.groupingBy(Student::getNota));
        System.out.println(map);

    }

}
