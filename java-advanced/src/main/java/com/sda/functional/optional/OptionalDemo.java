package com.sda.functional.optional;

import java.util.Optional;

public class OptionalDemo {
    public static void main(String[] args) {
        Optional<String> empty = Optional.empty();
        Optional<String> ana =Optional.of("ANA");
//        empty.get(); - exceptie pentru ca e empty
        if(empty.isPresent()){
            empty.get();
        }
        if(ana.isPresent()){
            ana.get();
        }
        empty.ifPresent(System.out::println);
        ana.ifPresent(System.out::println);
        String mihai = empty.orElse("Mihai");
        System.out.println(mihai);
        //throws exception becuase is empty optional
//        String str = empty.orElseThrow(RuntimeException::new);
        String s1 = ana.map(s -> s + "!").get();
        System.out.println(s1);
    }
}
