package com.sda.logger;

import java.util.logging.Logger;

public class LoggerDemo {

    private static final Logger logger = Logger.getLogger(LoggerDemo.class.getName());
    public static void main(String[] args) {
        try{
            int x=4;
            int y=1;
            int z=x/y;
            logger.info("First result "+z);
            int w=0;
            z=x/w;
        } catch (ArithmeticException e){
            logger.severe("Divide by 0");
            throw new MyException("Divide by 0");
        }
    }

    private static class MyException extends RuntimeException{
        MyException(String message){
            super(message);
        }
    }
}
