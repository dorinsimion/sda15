package com.sda.nested;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class InnerExemple {
    private String example;
    private String other;

    public InnerExemple(String example, String other) {
        this.example = example;
        this.other = other;
    }

    private interface InnerInterface{
        void doNothing();
    }

    public class InnerClass {
        private String example;

        public InnerClass(String example) {
            this.example = example;
        }

        public void display(String example){
            System.out.println(example);
            System.out.println(this.example);
            System.out.println(InnerExemple.this.example);
            System.out.println(other);
        }
    }

    public void display(){
        class Example{
            public void display(){
                System.out.println("test");
            }
        }
        new Example().display();
        System.out.println("method");
    }

    public Comparator<String> getComparator(){
        Comparator<String> comparator = new Comparator<String>() {
            public int compare(String o1, String o2) {
                return o2.compareTo(o1);
            }
        };

        Comparator<String> comparatorLamba =(o1,o2) -> o2.compareTo(o1);
        return comparator;
    }
}
