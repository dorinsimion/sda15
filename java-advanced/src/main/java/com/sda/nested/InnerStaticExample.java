package com.sda.nested;

public class InnerStaticExample {

    private String name;
    private String surname;

    private InnerStaticExample(){}

    public static class Builder {
        private InnerStaticExample example;

        public Builder() {
            this.example = new InnerStaticExample();
        }

        public Builder setName(String name){
            example.name=name;
            return this;
        }

        public Builder setSurname(String surname){
            example.surname=surname;
            return this;
        }

        public InnerStaticExample build(){
            return this.example;
        }
    }
}
