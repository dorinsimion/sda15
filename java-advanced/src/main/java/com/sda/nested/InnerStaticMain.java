package com.sda.nested;

import static com.sda.nested.InnerStaticExample.Builder;

public class InnerStaticMain {
    public static void main(String[] args) {
        Builder builder = new Builder();
        InnerStaticExample example = builder.setName("Ion")
                                            .setSurname("Mihai")
                                            .build();
    }
}
