package com.sda.nested;

public class InnerMain {
    public static void main(String[] args) {
        InnerExemple innerExemple = new InnerExemple("1","@");
        InnerExemple.InnerClass inner = innerExemple.new InnerClass("3");
        inner.display("$");
    }
}
