package com.sda.collections.queue;

import java.util.ArrayDeque;
import java.util.Queue;

public class FifoExample {
    public static void main(String[] args) {
        Queue<String> coada= new ArrayDeque<>();
        // offer/peek/poll
        coada.offer("Ion");
        coada.offer("Vasile");
        coada.offer("Marius");
        String x = coada.peek();
        System.out.println("Urmatorul la rand "+ x);
        String first = coada.poll();//Ion
        String second = coada.poll();//Vasile
        String third = coada.poll();//Marius
        String nuMaiENimeni = coada.poll();
        System.out.println(nuMaiENimeni); //null
        System.out.println("Mai sunt oameni la rand? "+!coada.isEmpty());
    }
}
