package com.sda.collections.set;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class BookExercise {
    public static void main(String[] args) {
        Set<Book> books = new HashSet<>();
        books.add(new Book("Ion"));
        books.add(new Book("Moara cu noroc"));
        books.add(new Book("Morometii"));

        System.out.println("Introduceti numele cartii:");
        Scanner scanner = new Scanner(System.in);
        String carte = scanner.nextLine();
        Book book= new Book(carte);
        if(books.contains(book)){
            System.out.println("Am citit cartea:"+ book);
            books.remove(book);
        } else {
            System.out.println("Nu avem cartea!");
        }
        System.out.println("Mai avem "+ books.size()+" carti de citit!");
        for (Book b:books)
            System.out.println(b);
    }
}
