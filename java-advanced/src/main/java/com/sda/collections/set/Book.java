package com.sda.collections.set;

import java.util.Objects;

public class Book {
    private String nume;

    public Book(String nume) {
        this.nume = nume;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return nume.equals(book.nume);
    }

    @Override
    public int hashCode() {
        return nume.hashCode();
    }

    @Override
    public String toString(){
        return nume;
    }
}
