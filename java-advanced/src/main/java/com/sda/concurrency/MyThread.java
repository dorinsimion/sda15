package com.sda.concurrency;

public class MyThread extends Thread {
    private int i;

    public MyThread(String name, int i) {
        super(name);
        this.i = i;
    }

    public void run(){
        for(int j=0;j<10;j++){
            i+=j;
            System.out.println(this.getName()+":"+i);
        }
    }
}
