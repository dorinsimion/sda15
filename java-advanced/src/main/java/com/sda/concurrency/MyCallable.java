package com.sda.concurrency;

import java.util.concurrent.Callable;

public class MyCallable implements Callable<String> {
    public String call() throws Exception {
        for(int i=0;i<10;i++)
            System.out.println("Hi"+i);
        return "Hi";
    }
}
