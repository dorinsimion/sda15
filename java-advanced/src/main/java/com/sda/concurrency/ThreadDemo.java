package com.sda.concurrency;

public class ThreadDemo {
    public static void main(String[] args) {
        Thread firstThread = new MyThread("Gigel",0);
        Thread secondThread = new MyThread("Ion",1);
        System.out.println("before");
        firstThread.start();
        System.out.println("between");
        secondThread.start();
        System.out.println("after");
    }
}
