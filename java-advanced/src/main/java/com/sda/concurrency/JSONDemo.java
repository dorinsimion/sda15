package com.sda.concurrency;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;

public class JSONDemo {
    private static Logger logger = LoggerFactory.getLogger(JSONDemo.class);
    public static void main(String[] args) throws IOException {
        ObjectMapper mapper= new ObjectMapper();
        Student s= new Student("Ion",4);
        mapper.writeValue(Files.newBufferedWriter(Paths.get("1.txt")),s);

        Student student = mapper.readValue(new File("1.txt"), Student.class);
        System.out.println(student.getNume());

        try {
            JAXBContext context = JAXBContext.newInstance(Student.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.marshal(student,new File("1.xml"));
        } catch (JAXBException e) {
            e.printStackTrace();
        }


        try {
            JAXBContext context = JAXBContext.newInstance(Student.class);
//            Marshaller marshaller = context.createMarshaller();
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Object unmarshal = unmarshaller.unmarshal(Files.newBufferedReader(Paths.get("1.xml")));
            System.out.println(((Student) unmarshal).getNota());
            logger.info("Nota studentului este {}",((Student) unmarshal).getNota());
        } catch (JAXBException| NoSuchFileException e) {
            logger.error("could't unmarshall file {} ","2.txt");

        }

        System.out.println("here");

    }
}
