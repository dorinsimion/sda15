package com.sda.concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class SyncronizedDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService service=null;
        List<Future<Integer>> futures=null;
        long start = System.currentTimeMillis();
        try{
            service= Executors.newFixedThreadPool(20);
            SheepManager manager = new SheepManager();
            List<Callable<Integer>> list = new ArrayList<>();
            for(int i=0;i<1000;i++) {
                list.add(manager::countAndReport);
                service.submit(manager::countAndReport);
            }
        }finally {
            if(service!=null){
                service.shutdown();
                long end= System.currentTimeMillis();
                System.out.println("time:"+(end-start));
            }
        }


    }
}
