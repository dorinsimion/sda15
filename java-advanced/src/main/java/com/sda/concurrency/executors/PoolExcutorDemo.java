package com.sda.concurrency.executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PoolExcutorDemo {
    public static void main(String[] args) {
        ExecutorService service=null;
        try{
            int cores = Runtime.getRuntime().availableProcessors();
            service= Executors.newFixedThreadPool(2*cores);
            service.submit(new MyCallable());
            service.submit(new MyCallable());
            service.submit(new MyCallable());
        }finally {
            if(service!=null){
                service.shutdownNow();
            }
        }
    }
}
