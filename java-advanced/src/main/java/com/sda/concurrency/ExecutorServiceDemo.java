package com.sda.concurrency;

import java.util.concurrent.*;

public class ExecutorServiceDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(4);
        Future<String> submit=null;
        for(int i=0;i<3;i++){
             submit = service.submit(new MyCallable());
            Future<?> hello = service.submit(() -> System.out.println("Hello"));
            hello.cancel(false);
//            service.invokeAll()
        }
        String s = submit.get();
        System.out.println(s);
        System.out.println("after");
        ScheduledExecutorService e= Executors.newSingleThreadScheduledExecutor();
        e.schedule(()-> System.out.println("Hello"),5,TimeUnit.SECONDS);
        e.scheduleAtFixedRate(()-> System.out.println("Hello1"),0,2,TimeUnit.SECONDS);

        Thread.sleep(10000);
        e.shutdown();
        service.shutdown();

    }
}
