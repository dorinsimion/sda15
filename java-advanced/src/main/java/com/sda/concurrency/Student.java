package com.sda.concurrency;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Student {

    private String nume;
    private int nota;

    public Student() {
    }

    public Student(String nume, int nota) {
        this.nume = nume;
        this.nota = nota;
    }

    @XmlAttribute
    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }
}
