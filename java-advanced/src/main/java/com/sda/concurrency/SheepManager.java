package com.sda.concurrency;

public class SheepManager {
    private int sheepCount=0;

     synchronized int countAndReport(){
         System.out.println((++sheepCount)+" found!");
        return sheepCount;
    }
}
