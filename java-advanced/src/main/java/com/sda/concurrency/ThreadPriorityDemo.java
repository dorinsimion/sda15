package com.sda.concurrency;

import java.util.ArrayList;
import java.util.List;

public class ThreadPriorityDemo {
    public static void main(String[] args) {
        int i = Runtime.getRuntime().availableProcessors();
        System.out.println(i);
        Thread t1 = new MyThread("q1",0);
        Thread t2 = new MyThread("q2",0);
        Thread t3 = new MyThread("q3",0);
        Thread t4 = new MyThread("q4",0);
        Thread t5 = new MyThread("q5",0);
        Thread t6 = new MyThread("q6",0);
        Thread t7 = new MyThread("q7",0);
        Thread t8 = new MyThread("q8",0);
        Thread t9 = new MyThread("q9",0);
        Thread t10 = new MyThread("q10",0);
        List<Thread> list = new ArrayList<>();
        for(int j=0;j<1000;j++){
            list.add(new MyThread("i",0));
            list.get(j).setPriority(Thread.MAX_PRIORITY);
            list.get(j).start();
        }
        Thread.currentThread().setPriority(8);
        t1.setPriority(Thread.MIN_PRIORITY);
        t2.setPriority(Thread.MIN_PRIORITY);
        t9.setPriority(Thread.MIN_PRIORITY);
        t4.setPriority(Thread.MAX_PRIORITY);
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
        t7.start();
        t8.start();
        t9.start();
        t10.start();


    }
}
