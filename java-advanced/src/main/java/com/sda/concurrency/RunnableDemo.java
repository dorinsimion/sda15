package com.sda.concurrency;

public class RunnableDemo {
    public static void main(String[] args) {
        System.out.println("0");
        new Thread(new MyRunnable()).start();
        System.out.println("1");
        new Thread(new MyRunnable()).start();
        System.out.println("2");
        new Thread(new MyRunnable()).start();
        System.out.println("3");
    }
}
