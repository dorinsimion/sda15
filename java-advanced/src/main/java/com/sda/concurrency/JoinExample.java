package com.sda.concurrency;

public class JoinExample implements Runnable{

    public void run() {
        for(int i=0;i<10;i++){
            System.out.println(Thread.currentThread().getName()+": Hi!");
            if(i==5){
                Thread thread = new Thread(new MyRunnable());
                thread.start();
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
