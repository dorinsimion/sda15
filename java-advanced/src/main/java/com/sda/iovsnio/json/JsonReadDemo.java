package com.sda.iovsnio.json;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JsonReadDemo {
    public static void main(String[] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Path file = Paths.get("student.json");
        Student student =
                mapper.readValue(Files.newBufferedReader(file),Student.class);
        System.out.println(student);
    }
}
