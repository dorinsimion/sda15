package com.sda.iovsnio.json;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JsonWriteDemo {
    public static void main(String[] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Student student = new Student("Ion",10);
        Path file = Paths.get("student.json");
        mapper.writeValue(Files.newBufferedWriter(file),student);
    }
}
