package com.sda.iovsnio.files;

import java.io.*;

public class ReadObjectFromFile {
    public static void main(String[] args) {
        try(InputStream low = new FileInputStream("student.data");
            BufferedInputStream high = new BufferedInputStream(low);
            ObjectInputStream in = new ObjectInputStream(high)){
                while(true){
                    Object o = in.readObject();
                    if( o instanceof String){
                        System.out.println("String: "+o);
                    }
                    if (o instanceof Student){
                        Student student = (Student) o;
                        System.out.println(student.getNota());
                    }
                }
        } catch(EOFException e){
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
