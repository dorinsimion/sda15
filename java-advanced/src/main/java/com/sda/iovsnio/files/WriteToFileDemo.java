package com.sda.iovsnio.files;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class WriteToFileDemo {
    public static void main(String[] args) {
        try (BufferedWriter out2= new BufferedWriter(new FileWriter("1/1.txt"))){
            out2.write("Ana");
            out2.newLine();
            out2.write("are");
            out2.newLine();
            out2.write("mere!");
        } catch (IOException e) {
            System.out.println("Nu s-a putut scrie in fisier!");
        }
    }
}
