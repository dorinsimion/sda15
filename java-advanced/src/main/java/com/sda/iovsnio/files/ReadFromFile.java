package com.sda.iovsnio.files;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadFromFile {
    public static void main(String[] args) {
        try(FileReader reader = new FileReader("1/1.txt");
            BufferedReader in = new BufferedReader(reader)){
            String line;
            while((line=in.readLine())!=null){
                System.out.println(line);
            }
        }catch (FileNotFoundException e){
            System.out.println("File not found!");
        }catch (IOException e){
            System.out.println("error reading fromfile");
        }
    }
}
