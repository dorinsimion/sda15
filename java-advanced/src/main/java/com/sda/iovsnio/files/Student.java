package com.sda.iovsnio.files;

import java.io.Serializable;

public class Student implements Serializable {
    private static final long serialVersionUID=1L;
    private String nume;
    transient private int nota;

    public Student(String nume, int nota) {
        this.nume = nume;
        this.nota = nota;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    @Override
    public String toString() {
        return nume+":"+nota;
    }
}
