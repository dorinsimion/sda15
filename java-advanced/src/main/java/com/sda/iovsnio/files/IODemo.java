package com.sda.iovsnio.files;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class IODemo {
    public static void main(String[] args) {
        File file = new File("1.txt");
        if(!file.exists()){
            System.out.println(file.getName()+ " nu exista");
        }

        File directory = new File("1");
        if(!directory.exists()){
            directory.mkdir();
        }
        if(directory.exists()){
            System.out.println(directory);
            System.out.println(directory.getAbsolutePath());
        }

        boolean delete = directory.delete();
        System.out.println("S-a sters?"+ delete);
        delete = directory.delete();
        System.out.println("S-a sters?"+ delete);

        File newFolder = new File("1/2");
        boolean mkdir = newFolder.mkdir();
        System.out.println(mkdir);
        boolean mkdirs = newFolder.mkdirs();
        System.out.println(mkdirs);
        newFolder.delete();

        File newFile = new File("1/1.txt");
        try {
            newFile.createNewFile();
        } catch (IOException e) {
            System.out.println("Eroare creare fisier");
        }

        if(newFile.isFile()){
            System.out.println("este fisier");
        }
        if(newFolder.isDirectory()){
            System.out.println("este folder");
        }
        if(newFile.canRead() && newFile.canWrite()){
            System.out.println("Drepturi citire/scriere");
        }

        long l = newFile.lastModified();
        System.out.println
                (LocalDateTime.ofInstant(Instant.ofEpochMilli(l), ZoneId.systemDefault()));
    }
}
