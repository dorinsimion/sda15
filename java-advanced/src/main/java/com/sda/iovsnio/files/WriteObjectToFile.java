package com.sda.iovsnio.files;

import java.io.*;

public class WriteObjectToFile {
    public static void main(String[] args) {
        Student student = new Student("Ion",2);
        try(OutputStream low = new FileOutputStream("student.data");
        BufferedOutputStream high = new BufferedOutputStream(low);
        ObjectOutputStream out = new ObjectOutputStream(high)){
            out.writeObject("Ion");
            out.writeObject(student);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
