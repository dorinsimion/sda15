package com.sda.iovsnio.nio;

import javax.print.DocFlavor;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class ReadAndWriteToFile {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("1/2.txt");
        List<String> lines = Files.readAllLines(path);
        System.out.println(lines);
        System.out.println("***");
        Files.lines(path).sorted().forEach(System.out::println);

        System.out.println("****");
        try(BufferedReader in = Files.newBufferedReader(path)){
//            String line;
//            while((line=in.readLine())!=null){
//                if(line.toUpperCase().startsWith("A")){
//                    System.out.println(line);
//                }
//            }
            System.out.println("****");
            in.lines()
                    .filter(x->x.toUpperCase().startsWith("A"))
                    .forEach(System.out::println);
        }

        path = Paths.get("1/1.txt");
        try(BufferedWriter out = Files.newBufferedWriter(path,StandardOpenOption.CREATE, StandardOpenOption.APPEND)){
            out.newLine();
            out.write("Ana");
        }

        List<String> list = new ArrayList<>();
        list.add("Anaa");
        Files.write(path,list,StandardOpenOption.APPEND);

        try(FileWriter low = new FileWriter("1/2.txt",true);
        BufferedWriter out = new BufferedWriter(low)){
            out.write("Ana");
        }
    }

}
