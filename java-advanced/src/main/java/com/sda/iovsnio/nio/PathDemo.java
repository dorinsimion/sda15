package com.sda.iovsnio.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.DosFileAttributeView;
import java.nio.file.attribute.UserPrincipal;
import java.util.List;

public class PathDemo {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("1/1.txt");
        if(path.isAbsolute()){
            System.out.println(path);
        } else{
            System.out.println(path.getRoot());
            System.out.println(path.getParent());
            System.out.println("Is not!");
            Path pathAbsolute = path.toAbsolutePath();
            System.out.println(pathAbsolute);
            System.out.println(pathAbsolute.getRoot());
            System.out.println(pathAbsolute.getParent());
            System.out.println(pathAbsolute.getFileName());
        }

        System.out.println(path);
        Path currentDir = Paths.get("1","2","2.txt");
        Path relativize = currentDir.relativize(path);
        System.out.println(relativize);
        System.out.println(relativize.normalize());

        boolean exists = Files.exists(path);
        System.out.println(path+" exists? "+ exists);
        System.out.println(Files.exists(path));
        try {
            Files.deleteIfExists(currentDir);
            if(!Files.exists(Paths.get("1/2")))
                Files.createDirectory(Paths.get("1/2"));
            Files.copy(path,currentDir);
            if(Files.notExists(Paths.get("1/2.txt")))
            Files.move(currentDir,Paths.get("1/2.txt"));
            UserPrincipal owner = Files.getOwner(path);
            System.out.println(owner.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<String> strings = Files.readAllLines(path);
        strings.forEach(System.out::println);

    }
}
