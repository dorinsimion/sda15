package com.sda.iovsnio.xml;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public class XmlWriteDemo {
    public static void main(String[] args) {
        try{
            JAXBContext context = JAXBContext.newInstance(Student.class);
            Marshaller marshaller = context.createMarshaller();
            Student student = new Student("Ion",4);
            marshaller.marshal(student,new File("student.xml"));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
