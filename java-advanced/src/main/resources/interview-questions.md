1. Ce valoare returneaza un constructor?
2. De ce constructorii nu pot fii final,static sau abstract?
3. Ce reprezinta keyword ul this?
4. Care este clasa pe care o extind toate obiectele?
5. Ce inseamna composition?
6. Ce inseamna inheritance?
7. Ce reprezinta NullPointerException?
8. Putem folosi this() si super() in acelasi constructor?
9. Ce reprezinta static?
10. Ce inseamna overloading? dar override?
11. De ce nu putem face overloading doar prin schimbarea tipului returnat?
12. Cum trebuie sa fie tipul returnat intr-o metoda suprascrisa?
13. Cum trebuie sa fie modificatorul de acces intr-o metoda suprascrisa
14. Ce e polimorfismul?
15. Pot avea o clasa abstracta fara nicio metoda abstracta? Dar o clasa neabstracta cu metode abstracte?
16. De ce metodele abstracte nu pot fi final?
17. Ce e o interfata marker? Dati un exemplu de interfata marker.
18. Diferente dintre interfete si clase abstracte.
19. Cum putem interzice mostenirea unei metode?
20. Cum putem schimba valoarea unei variabile final?
21. Unde pot initializa o variabila final?
22. Unde pot avea modificatorul final?
23. Diferenta intre final si finally.
24. Care e cea mai folosita clasa din Java?
25. Diferenta intre import com.test.Foo si import static com.test.Foo.*
26. Mentionati 2 clase de lucru cu date/timp in Java.
27. De ce am marca un membru al clasei cu transient?
28. Putem avea metode transient? In ce situatii sau de ce nu ?
29. Ce e Garbage Collector?
30. Ce sunt clasele nested?
31. Ce e o clasa anonima?
32. Ce inseamna immutable?
33. Diferente intre String/StringBuilder
34. Ce face metoda toString
35. Diferentele dintre checked si unchecked exceptions
36. Putem avea finally fara catch? Dar invers?
37. Ce reprezinta try-with-resources?
38. Ce obiecte putem avea intre parantezele unui bloc try-with-resources
39. Diferenta intre throw si throws
40. Ce inseamna propagarea exceptiilor
41. Cand facem override putem avea in child class exceptie daca in parinte metoda nu arunca exceptie?
42. Diferenta intre Collection si Collections
43. Diferenta intre List,Set, Map
44. Diferenta intre HashMap si TreeMap
45. Comparable vs Comparator?
46. Ce metode trebuie sa avem in obiectele care fac parte dintr-un HashSet
47. Cum putem sorta o lista de obicte care nu implementeaza Comparable
48. Ce metoda trebuie sa implementam daca implementam Comparable
49. Ce sunt Wrapper class? Ce inseamna Autoboxing?
50. Cum eliminam duplicatele dintr-un ArrayList?
51. Cum putem itera o lista?
52. Ce se intampla cand apelam metoda put cu aceeasi cheie de mai multe ori intr-un map
52. Cum stergem elementul de pe prima pozitie dintr-un set?
53. Scrieti o metoda care primul numar 1 dintr-o ArrayList de Integer.
54. De ce un set nu are metoda sa adaugam un element la o anumita pozitie?
55. Ce este o stiva? Dar o coada?
56. Ce face @FunctionalInterface ?
57. Ce e o operatie intermediara si o operatie terminala intr-un Stream
58. Ce este un Predicate?
59. Ce este un Supplier? Dar un Consumer?
60. Diferenta intre Function si UnaryOperator.
61. Ce primeste ca parametru un map intr-un stream? Dar metoda filter? Dar forEach?
62. Cate operatii terminale avem intr-un stream?
63. De cate ori putem folosi un stream?
64. Ce este o metoda default?
65. Cate metode default pot exista intr-o functional interface?
66. IS-A vs HAS-A
67. Ce este Optional?
68. Care e diferenta intre x==y si x.equals(y)
69. Ce reprezinta compile time? Dar runtime?
70. O annotation de compile si una de runtime.
71. Ce e un multi-catch? Cum trebuie sa fie exceptiile dintr-un multi-catch?
72. Care este ordinea exceptiilor intr-un try cu mai multe catch?
73. String s="Hi"; String t= new String("Hi"); String u="Hi"; Ce returneaza s==t; s==u; s.equals(t); s.equals(u);
74. Ce este encapsularea?
75. Ce e git?
76. Ce annotation folosim pentru o metoda de test? Dar pentru o metoda care vrem sa ruleze inainte de fiecare test?
77. Ce e un mock?
78. Ce comanda folosim pentru a face package la un proiect maven?
79. Ce face git pull? Dar git diff? Dar git status?
80. Care sunt principiile de baza OOP?
81. Ce returneaza (Integer) 222 != (Integer) 222?
82. Ce este un low level stream? Dar high level?
83. Ce este un input stream? Dar un output stream?
84. Avem doua obiecte de acelasi tip, a si b. Daca a.compareTo(b) este 5. Cat este b.compareTo(a)?
85. Ce exceptii poate arunca citire/scrierea intr-un fisier.
86. cum scriem () -> new TreeMap<>; ca method refference. Dar (s) -> "A".equals(s). Dar (s1,s2)-> s1.equals(s2)?
87. Ce functional interface folosesti daca vrei sa verifici ca lungimea unui sir este cu un numar.
88. Ce se intampla daca adaugam intr-un set obiecte care nu au implementare de hascode si equals?
89. Ce se poate intampla daca avem in hascode verificari pe fielduri care nu exista in equals?
90. ce e javac? Ce extensie are un fisier in Java?
..........................................
91. Un program care intoarce un String.Cu si fara JAVA API.
92. Verificati ca un numar este prim.
93. Gasiti al 2 lea cel mai mare numar dintr-o lista
94. Calculat factorial de n
95. Calculati fibonnaci
96. Afiseaza tabla inmultirii de la 1 la 10.
97. Cititi dintr-un fisier si afisati liniile care incep cu litera A
98. Verificati daca exista 2 numere intr-o lista a caror suma este 100.
99. Stergeti toate numerele 0 dintr-o lista(3 moduri)
100. Afisati numarul de cuvinte dintr-o lista care au lungime para.
