package com.sda.hibernate.mapping;

public class StudentDemo2 {
    public static void main(String[] args) {
        Student s = new Student();
        s.setGender(Gender.M);
        s.setName("Ion");
        StudentRepository repo = StudentRepository.getInstance();
        repo.addNewStudent(s);
        repo.updateStudent(1);

    }
}
