package com.sda.hibernate.mapping.config;

import com.sda.hibernate.mapping.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateUtil {
   private static final SessionFactory INSTACE = getSessionFactory(getConfiguration());

   private HibernateUtil(){}

   public static SessionFactory getInstace(){
       return INSTACE;
   }

   private static SessionFactory getSessionFactory(Configuration configuration){
        ServiceRegistry serviceRegistry =
                new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
        return configuration.buildSessionFactory(serviceRegistry);
    }

    private static Configuration getConfiguration(){
        Configuration configuration = new Configuration();
        Properties settings = new Properties();
        settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
        settings.put(Environment.URL, "jdbc:mysql://localhost:3306/hibernate");
        settings.put(Environment.USER, "root");
        settings.put(Environment.PASS, "admin123");
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL8Dialect");
        settings.put(Environment.SHOW_SQL, "true");
        settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        settings.put(Environment.HBM2DDL_AUTO, "create");
        configuration.setProperties(settings);

        configuration.addAnnotatedClass(Student.class);
        configuration.addAnnotatedClass(StudentInfo.class);
        configuration.addAnnotatedClass(Nota.class);
        configuration.addAnnotatedClass(Course.class);

        configuration.addAnnotatedClass(Programmer.class);
        configuration.addAnnotatedClass(SysAdmin.class);

        return configuration;
    }
}
