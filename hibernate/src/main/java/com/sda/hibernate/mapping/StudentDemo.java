package com.sda.hibernate.mapping;

import java.time.LocalDate;

public class StudentDemo {
    public static void main(String[] args) {
        StudentRepository repo = StudentRepository.getInstance();
        Student s = new Student();
        s.setName("Gigi");
        s.setBirthday(LocalDate.of(1998,9,9));
        repo.addStudent(s);

        repo.getById(1).ifPresent(System.out::println);

//        repo.deleteStudent(1);
        s = new Student();
        s.setName("Ion");
        s.setId(1);
        repo.updateStudent(s);
        s = new Student();
        s.setName("Hagi");
        s.setGender(Gender.M);
        repo.addStudent(s);

        s = new Student();
        s.setGender(Gender.M);
        s.setName("Ion");
        StudentInfo studentInfo = new StudentInfo();
        Address a = new Address();
        a.setStreet("Bucuresti");
        studentInfo.setAddress(a);
        s.setStudentInfo(studentInfo);
        studentInfo.setStudent(s);
        repo.addStudent(s,studentInfo);

        s = new Student();
        s.setName("Mitica");
        Nota n = new Nota();
        n.setNota(10);
        s.addNota(n);
        n=new Nota();
        n.setNota(6);
        s.addNota(n);
        repo.addGrades(s);
        System.out.println(repo.getById(4));
    }
}
