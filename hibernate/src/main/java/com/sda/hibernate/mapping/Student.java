package com.sda.hibernate.mapping;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name ="students")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name="full_name")
//   @Basic(fetch = FetchType.LAZY)
    private String name;

    private LocalDate birthday;

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    @ManyToMany
    @JoinTable(name ="student_course",
            joinColumns =@JoinColumn(name="student_id"),
        inverseJoinColumns =@JoinColumn(name = "course_id") )
    private List<Course> courses;

    @OneToMany(mappedBy = "student",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<Nota> notaList = new ArrayList<>();

    void addNota(Nota nota){
        notaList.add(nota);
        nota.setStudent(this);
    }

    public List<Nota> getNotaList() {
        return notaList;
    }

    public void setNotaList(List<Nota> notaList) {
        this.notaList = notaList;
    }

    @OneToOne(mappedBy = "student",cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    private StudentInfo studentInfo;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    public StudentInfo getStudentInfo() {
        return studentInfo;
    }

    public void setStudentInfo(StudentInfo studentInfo) {
        this.studentInfo = studentInfo;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return id +". " + name + " - " + birthday +":"+notaList;
    }
}
