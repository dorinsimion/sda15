package com.sda.hibernate.mapping;

import javax.persistence.Embeddable;

@Embeddable
public class Address {

    private String street;

    private String no;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }
}
