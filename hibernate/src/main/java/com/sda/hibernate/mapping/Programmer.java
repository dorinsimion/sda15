package com.sda.hibernate.mapping;

import javax.persistence.*;

@Entity
@AttributeOverrides(
        @AttributeOverride(name ="firstName", column = @Column(name="first_name"))
)
public class Programmer extends Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name ="language")
    private  String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
