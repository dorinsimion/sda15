package com.sda.hibernate.mapping;

import com.sda.hibernate.mapping.config.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.LazyInitializationException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.Optional;

public class StudentRepository {

    private static final SessionFactory SESSION_FACTORY= HibernateUtil.getInstace();
    private static final StudentRepository INSTANCE = new StudentRepository();

    private StudentRepository(){}

    static StudentRepository getInstance(){
        return INSTANCE;
    }

    void addStudent(Student s){
        try (Session session = SESSION_FACTORY.openSession()) {
            session.beginTransaction();
            session.save(s);
            session.getTransaction().commit();
        }
    }

    Optional<Student> getById(int id){
        try(Session session = SESSION_FACTORY.openSession()){
            Student student = session.find(Student.class, id);
            return student==null ?
                    Optional.empty() : Optional.of(student);
        }
    }

    void deleteStudent(int id){
        try(Session session = SESSION_FACTORY.openSession()){
            session.beginTransaction();
            Optional<Student> byId = getById(id);
            byId.ifPresent(student -> session.delete(student));
            session.getTransaction().commit();
        }
    }

    void updateStudent(Student s){
        try(Session session = SESSION_FACTORY.openSession()){
            session.beginTransaction();
            Optional<Student> byId = getById(s.getId());
            byId.ifPresent(student -> student.setName(s.getName()));
            byId.ifPresent(session::update);
            session.getTransaction().commit();
        }
    }

    void addStudent(Student s, StudentInfo studentInfo){
        try(Session session =SESSION_FACTORY.openSession()){
            session.beginTransaction();
//            session.persist(s);
            session.save(studentInfo);
            session.getTransaction().commit();
        }
    }

    void addGrades(Student s){
        try(Session session = SESSION_FACTORY.openSession()){
            session.beginTransaction();
            session.save(s);
            session.getTransaction().commit();
        }
    }

    void addNewStudent(Student student){
        try(Session session = SESSION_FACTORY.openSession()){
            session.beginTransaction();
            session.save(student);

            student.setGender(Gender.F);
            session.flush();
//            if(true)
//                throw new RuntimeException();
            student.setName("Gigi");
            session.getTransaction().commit();
        }
    }

    void updateStudent(int id){
        try(Session session = SESSION_FACTORY.openSession()){
            session.beginTransaction();
            Student student = session.find(Student.class, id);

            student.setName("Ionel");
            System.out.println(student.getName());//Ionel
            // se sincronizeaza cu baza de date si updateaza cu datele din baza
            session.refresh(student);
            System.out.println(student.getName());//Gigi
            // sterge obiectul din context
            session.evict(student);
            // sterge toate obiectele din context
            session.clear();
            student.setName("Mihai");
            session.update(student);
            session.getTransaction().commit();
        }
    }

    void addWithException(Student s){
        Session session =null;
        try{
            session = SESSION_FACTORY.openSession();
            session.beginTransaction();
            session.save(s);
            session.getTransaction().commit();
        } catch (HibernateException e){
            if(session!=null)
                session.getTransaction().rollback();
        }finally {
            if(session!=null)
                session.close();
        }
    }
}
