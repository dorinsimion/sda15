package com.sda.hibernate.mapping;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Employee {

    private String firstName;
    private String lastName;
    private int salary;

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
