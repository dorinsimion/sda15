package com.sda.hibernate.queries;

import lombok.*;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="owner")
@NamedNativeQueries(
        {@NamedNativeQuery(name="findAll",query = "SELECT * FROM owner",resultClass = Owner.class)}
)
@NamedQueries(
        @NamedQuery(name ="findAll2",query = "FROM Owner")
)
@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
//@Builder
public class Owner {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
//    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private List<Property> propertyList = new ArrayList<>();

    public static OwnerBuilder builder(){
        return new OwnerBuilder();
    }

    public static class OwnerBuilder {
        private Owner owner = new Owner();

        public OwnerBuilder id(int id){
            owner.setId(id);
            return this;
        }

        public OwnerBuilder firstName(String firstName){
            owner.setFirstName(firstName);
            return this;
        }

        public OwnerBuilder lastName(String lastName){
            owner.setLastName(lastName);
            return this;
        }

        public OwnerBuilder property(Property property){
            owner.getPropertyList().add(property);
            property.setOwner(owner);
            return this;
        }

        public Owner build(){
            return owner;
        }
    }
}
