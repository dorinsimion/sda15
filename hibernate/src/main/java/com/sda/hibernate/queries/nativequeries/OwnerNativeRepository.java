package com.sda.hibernate.queries.nativequeries;

import com.sda.hibernate.queries.Owner;
import com.sda.hibernate.queries.Property;
import com.sda.hibernate.queries.config.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;

import java.math.BigInteger;
import java.util.List;

public class OwnerNativeRepository {
    private static final SessionFactory SESSION_FACTORY
            = HibernateUtil.getSessionFactory();

    public List<Owner> findAll(){
        try(Session session = SESSION_FACTORY.openSession()){
            NativeQuery<Owner> sqlQuery = session.createSQLQuery("SELECT * FROM owner").addEntity(Owner.class);
            List<Owner> list = sqlQuery.list();
            return  list;
        }
    }

    public Owner findByName(String name){
        try(Session session = SESSION_FACTORY.openSession()){
            NativeQuery<Object[]> nativeQuery = session.createSQLQuery("SELECT o.*,p.* FROM owner o LEFT JOIN property p on o.id = p.owner_id WHERE o.first_name = :name")
                    .addEntity("o",Owner.class).addJoin("p","o.propertyList");
            nativeQuery.setParameter("name",name);
            Owner owner = (Owner)nativeQuery.list().get(0)[0];
            Property property = (Property) nativeQuery.list().get(0)[1];
            System.out.println(property.getOwner().getFirstName());
            return owner;
        }
    }

    public Owner findByLastName(String name){
        try(Session session = SESSION_FACTORY.openSession()){
            NativeQuery<Owner> nativeQuery = session.createNativeQuery("SELECT * FROM owner", Owner.class);
            return  nativeQuery.uniqueResult();
        }
    }

    public long getCount(){
        try(Session session = SESSION_FACTORY.openSession()){
            String sql = "SELECT count(*) FROM property";
            NativeQuery<BigInteger> nativeQuery = session.createNativeQuery(sql);
            BigInteger aLong = nativeQuery.uniqueResult();
            return Long.valueOf(aLong+"");
        }
    }

    public void displayOwners(){
        try(Session session = SESSION_FACTORY.openSession()){
            NativeQuery<Owner> findAll = session.getNamedNativeQuery("findAll");
            List<Owner> list = findAll.list();
            for(Owner o: list){
                System.out.println(o.getFirstName()+" "+o.getLastName());
            }
        }
    }
}
