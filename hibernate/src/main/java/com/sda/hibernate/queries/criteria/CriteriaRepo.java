package com.sda.hibernate.queries.criteria;

import com.sda.hibernate.queries.Owner;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class CriteriaRepo {

    private Session session;

    public CriteriaRepo(Session session) {
        this.session = session;
    }

    public void findByName(String name){
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Owner> query = cb.createQuery(Owner.class);
            Root<Owner> from = query.from(Owner.class);
            query.where(cb.equal(from.get("firstName"),name));
            Query<Owner> query1 = session.createQuery(query);
            Owner owner = query1.uniqueResult();
            System.out.println(owner.getLastName());
    }

    public void findByFirstNameAndLastNameOrderAsc(String firstName, String lastName){
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Owner> query = cb.createQuery(Owner.class);
            Root<Owner> from = query.from(Owner.class);
            query.select(from);
            query.where(cb.and(cb.equal(from.get("firstName"),firstName),
                                cb.equal(from.get("lastName"),lastName)))
                    .orderBy(cb.asc(from.get("firstName")),
                                cb.desc(from.get("lastName")));
            Query<Owner> query1 = session.createQuery(query);
            query1.list().forEach(s-> System.out.println(s.getFirstName()));
    }

    public void getCount(){
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Long> query = cb.createQuery(Long.class);
            Root<Owner> from = query.from(Owner.class);
            query.select(cb.count(from));
//            query.multiselect(from.get("firstName"),cb.count(from));
            Query<Long> query1 = session.createQuery(query);
            System.out.println(query1.uniqueResult());
    }


}
