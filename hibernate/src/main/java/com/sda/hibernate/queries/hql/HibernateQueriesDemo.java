package com.sda.hibernate.queries.hql;

import com.sda.hibernate.queries.Owner;
import com.sda.hibernate.queries.OwnerRepository;
import com.sda.hibernate.queries.Property;
import com.sda.hibernate.queries.PropertyType;

public class HibernateQueriesDemo {
    public static void main(String[] args) {
        Property p1 = Property.builder()
                .name("AP1")
                .type(PropertyType.FLAT)
                .build();
        Property p2 = Property.builder()
                .name("House1")
                .type(PropertyType.HOUSE)
                .build();
        Owner owner = Owner.builder()
                .firstName("Dorin")
                .lastName("Simion")
                .property(p1)
                .property(p2)
                .build();

        OwnerRepository repo = new OwnerRepository();
        repo.addOwner(owner);

        HibernateQueriesRepository repo2 = new HibernateQueriesRepository();
        repo2.findAll().forEach(s-> System.out.println(s.getFirstName()));

        System.out.println(repo2.findByFirstName("Dorin").getLastName());
        repo2.displayOwnerByName();
        repo2.joinFindAll();
        repo2.namedQuery();
    }
}
