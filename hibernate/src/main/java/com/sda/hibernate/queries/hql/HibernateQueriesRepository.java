package com.sda.hibernate.queries.hql;

import com.sda.hibernate.queries.Owner;
import com.sda.hibernate.queries.config.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

public class HibernateQueriesRepository {
    private static final SessionFactory SESSION_FACTORY=
            HibernateUtil.getSessionFactory();

    public List<Owner> findAll(){
        try(Session session = SESSION_FACTORY.openSession()){
            Query<Owner> ownerList = session.createQuery("FROM Owner", Owner.class);
            List<Owner> list = ownerList.list();
            return list;
        }
    }

    public Owner findByFirstName(String firstName){
        try(Session session = SESSION_FACTORY.openSession()){
            Query<Owner> query = session.createQuery("FROM Owner o WHERE o.firstName = :name", Owner.class);
            query.setParameter("name",firstName);
            return query.uniqueResult();
        }
    }

    public void displayOwnerByName(){
        try(Session session = SESSION_FACTORY.openSession()){
            String sql ="SELECT o.firstName, count(o) FROM Owner o group by o.firstName";
            Query<Object[]> query = session.createQuery(sql,Object[].class);
            List<Object[]> list = query.list();
            for(Object[] o : list){
                System.out.println("First Name: "+o[0]);
                System.out.println(o[1] instanceof Long);
                long count = (long) o[1];
                System.out.println(count);
                System.out.println("Count "+ o[1]);
            }
        }
    }

    public void joinFindAll(){
        try(Session session = SESSION_FACTORY.openSession()){
            Query<Owner> from_owner = session.createQuery("SELECT o FROM Owner o LEFT JOIN FETCH o.propertyList",Owner.class);
            Owner owner = from_owner.uniqueResult();
            System.out.println("-- Find all --");
            System.out.println(owner.getFirstName());
            System.out.println(owner.getPropertyList().size());
        }
    }

    public void namedQuery(){
        try(Session session = SESSION_FACTORY.openSession()){
            Query<Owner> findAll2 = session.getNamedQuery("findAll2");
            findAll2.list().forEach(s-> System.out.println(s.getId()));
        }
    }
}
