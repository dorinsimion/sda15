package com.sda.hibernate.queries;

import javax.persistence.*;

@Entity
public class Property {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name ="property_name")
    private String name;

    @ManyToOne
    @JoinColumn(name ="owner_id")
    private Owner owner;

    @Enumerated(EnumType.STRING)
    private PropertyType type;

    private long value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public PropertyType getType() {
        return type;
    }

    public void setType(PropertyType type) {
        this.type = type;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public static PropertyBuilder builder(){
        return new PropertyBuilder();
    }

    public static class PropertyBuilder{
        private Property property = new Property();

        public PropertyBuilder id(int id){
            property.setId(id);
            return this;
        }

        public PropertyBuilder name(String name){
            property.setName(name);
            return this;
        }

        public PropertyBuilder owner(Owner owner){
            property.setOwner(owner);
            owner.getPropertyList().add(property);
            return this;
        }

        public PropertyBuilder type(PropertyType type){
            property.setType(type);
            return this;
        }

        public PropertyBuilder value(long value){
            property.setValue(value);
            return this;
        }

        public Property build(){
            return property;
        }
    }
}
