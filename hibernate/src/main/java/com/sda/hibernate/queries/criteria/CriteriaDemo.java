package com.sda.hibernate.queries.criteria;

import com.sda.hibernate.queries.Owner;
import com.sda.hibernate.queries.OwnerRepository;
import com.sda.hibernate.queries.Property;
import com.sda.hibernate.queries.PropertyType;

public class CriteriaDemo {
    public static void main(String[] args) {
        Property p1 = Property.builder()
                .name("AP1")
                .type(PropertyType.FLAT)
                .build();
        Property p2 = Property.builder()
                .name("House1")
                .type(PropertyType.HOUSE)
                .build();
        Owner owner = Owner.builder()
                .firstName("Dorin")
                .lastName("Simion")
                .property(p1)
                .property(p2)
                .build();

        OwnerRepository repo = new OwnerRepository();
        repo.addOwner(owner);

//        CriteriaRepo repo2 = new CriteriaRepo();
//        repo2.findByName("Dorin");
//        repo2.findByFirstNameAndLastNameOrderAsc("Dorin","Simion");
//        repo2.getCount();
        CriteriaService criteriaService = new CriteriaService();
        criteriaService.findByNameAndGetCount();
    }
}
