package com.sda.hibernate.queries.config;

import com.sda.hibernate.queries.Owner;
import com.sda.hibernate.queries.Property;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateUtil {

    private static SessionFactory SESSION_FACTORY;

    public static synchronized SessionFactory getSessionFactory(){
        if(SESSION_FACTORY==null) {
            Configuration configuration = getConfiguration();
            ServiceRegistry serviceRegistry =
                    new StandardServiceRegistryBuilder()
                            .applySettings(configuration.getProperties()).build();
            SESSION_FACTORY = configuration.buildSessionFactory(serviceRegistry);
        }
        return SESSION_FACTORY;
        }
    private static Configuration getConfiguration(){
        Configuration configuration = new Configuration();
        Properties settings = new Properties();
        settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
        settings.put(Environment.URL, "jdbc:mysql://localhost:3306/hibernate_queries");
        settings.put(Environment.USER, "root");
        settings.put(Environment.PASS, "admin123");
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL8Dialect");
        settings.put(Environment.SHOW_SQL, "true");
        settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        settings.put(Environment.HBM2DDL_AUTO, "create");
        configuration.setProperties(settings);

        configuration.addAnnotatedClass(Owner.class);
        configuration.addAnnotatedClass(Property.class);
        return configuration;
    }
}
