package com.sda.hibernate.queries;

import com.sda.hibernate.queries.config.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class OwnerRepository {

    private static final SessionFactory SESSION_FACTORY
            =HibernateUtil.getSessionFactory();

    public void addOwner(Owner o){
        try(Session session = SESSION_FACTORY.openSession()){
            session.beginTransaction();
            session.save(o);
            session.getTransaction().commit();
        }

    }
}
