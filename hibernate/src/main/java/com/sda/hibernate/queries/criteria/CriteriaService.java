package com.sda.hibernate.queries.criteria;

import com.sda.hibernate.queries.config.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CriteriaService {
    private static final SessionFactory SESSION_FACTORY
            = HibernateUtil.getSessionFactory();

    public void findByNameAndGetCount(){
        try(Session session = SESSION_FACTORY.openSession()){
            CriteriaRepo criteriaRepo = new CriteriaRepo(session);
            criteriaRepo.findByName("Dorin");
            criteriaRepo.getCount();
        }
    }
}
