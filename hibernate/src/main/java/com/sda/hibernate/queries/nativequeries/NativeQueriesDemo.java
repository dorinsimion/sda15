package com.sda.hibernate.queries.nativequeries;

import com.sda.hibernate.queries.Owner;
import com.sda.hibernate.queries.OwnerRepository;
import com.sda.hibernate.queries.Property;
import com.sda.hibernate.queries.PropertyType;

public class NativeQueriesDemo {
    public static void main(String[] args) {
        Property p1 = Property.builder()
                .name("AP1")
                .type(PropertyType.FLAT)
                .build();
        Property p2 = Property.builder()
                .name("House1")
                .type(PropertyType.HOUSE)
                .build();
        Owner owner = Owner.builder()
                .firstName("Dorin")
                .lastName("Simion")
                .property(p1)
                .property(p2)
                .build();

        OwnerRepository repo = new OwnerRepository();

        repo.addOwner(owner);

        OwnerNativeRepository repo2 = new OwnerNativeRepository();
        repo2.findAll().forEach(o-> System.out.println(o.getFirstName()));

        System.out.println(repo2.findByName("Dorin").getLastName());
//        System.out.println(repo2.findByName(""));
        System.out.println(repo2.findByName("Dorin").getPropertyList().size());
        System.out.println(repo2.findByLastName("Simion").getFirstName());
        System.out.println(repo2.getCount());
    }
}
