package com.sda.hibernate.ex1.project;

import org.hibernate.Session;

public class ProjectRepository {

    private Session session;

    ProjectRepository() {
    }

    void save(Project project){
        session.save(project);
    }

    void setSession(Session session) {
        this.session = session;
    }

    Project findById(int id) {
        return session.find(Project.class,id);
    }

    void delete(Project project) {
        session.delete(project);
    }
}
