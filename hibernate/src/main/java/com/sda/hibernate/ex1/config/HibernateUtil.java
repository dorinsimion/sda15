package com.sda.hibernate.ex1.config;

import com.sda.hibernate.ex1.department.Department;
import com.sda.hibernate.ex1.employee.Employee;
import com.sda.hibernate.ex1.project.Project;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateUtil {

    private static SessionFactory SESSION_FACTORY;

    public static synchronized SessionFactory getSessionFactory(){
        if(SESSION_FACTORY==null) {
            Configuration configuration = getConfiguration();
            ServiceRegistry serviceRegistry =
                    new StandardServiceRegistryBuilder()
                            .applySettings(configuration.getProperties()).build();
            SESSION_FACTORY = configuration.buildSessionFactory(serviceRegistry);
        }
        return SESSION_FACTORY;
        }
    private static Configuration getConfiguration(){
        Configuration configuration = new Configuration();
        Properties settings = new Properties();
        settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
        settings.put(Environment.URL, "jdbc:mysql://localhost:3306/human_resources_exercise");
        settings.put(Environment.USER, "root");
        settings.put(Environment.PASS, "admin123");
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL8Dialect");
        settings.put(Environment.SHOW_SQL, "true");
        settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        settings.put(Environment.HBM2DDL_AUTO, "none");
        configuration.setProperties(settings);

        configuration.addAnnotatedClass(Employee.class);
        configuration.addAnnotatedClass(Department.class);
        configuration.addAnnotatedClass(Project.class);
        return configuration;
    }
}
