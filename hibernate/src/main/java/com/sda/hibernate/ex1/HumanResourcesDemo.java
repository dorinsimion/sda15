package com.sda.hibernate.ex1;

import com.sda.hibernate.ex1.employee.EmployeeRepository;
import com.sda.hibernate.ex1.project.Project;
import com.sda.hibernate.ex1.project.ProjectRepository;
import com.sda.hibernate.ex1.project.ProjectService;

public class HumanResourcesDemo {

    private static final ProjectService projectService = new ProjectService();
    private static final EmployeeRepository employeeRepository = new EmployeeRepository();
    public static void main(String[] args) {
        Project p1 = createProject("Java");
        Project p2 = createProject("Python");
//        projectService.save(p1);
//        projectService.save(p2);

        System.out.println(projectService.findById(2));
//        projectService.delete(2);

        System.out.println(projectService.findById(1));

//        projectService.update(1,"Angular");

        System.out.println(projectService.findById(1));

        employeeRepository.findAllNative().forEach(e -> System.out.println(e.getFirstName()));
        employeeRepository.findAllNative().forEach(e -> System.out.println(e.getProjects().size()));

        employeeRepository.findAllNativeNoJoin().forEach(e -> System.out.println(e.getFirstName()));
        // this doesn't work because no join (LazyInitialization becuase fetch is Lazy for collections)
        //        employeeRepository.findAllNativeNoJoin().forEach(e -> System.out.println(e.getProjects().size()));

        employeeRepository.findAllHQL().forEach(e-> System.out.println(e.getEmail()));
        employeeRepository.findAllCriteria().forEach(employee -> System.out.println(employee.getManager()==null ? null: employee.getManager().getFirstName()));

        employeeRepository.findByDepartmentNative("Finance");
        employeeRepository.findByDepartmentHQL("Finance");
        employeeRepository.findByDepartmentCriteria("Finance");
    }

    private static Project createProject(String description){
        Project project = new Project();
        project.setDescription(description);
        return project;
    }
}
