package com.sda.hibernate.ex1.employee;

import com.sda.hibernate.ex1.config.HibernateUtil;
import com.sda.hibernate.ex1.department.Department;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;

import javax.persistence.criteria.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EmployeeRepository {

    private static  final SessionFactory SESSION_FACTORY = HibernateUtil.getSessionFactory();

    public List<Employee> findAllHQL(){
        try(Session session = SESSION_FACTORY.openSession()){
            Query<Employee> query = session.createQuery("SELECT e from Employee e", Employee.class) ;
            return  query.list();
        }
    }

    public List<Employee> findAllCriteria(){
        try(Session session = SESSION_FACTORY.openSession()){
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Employee> query = cb.createQuery(Employee.class);
            Root<Employee> root = query.from(Employee.class);
            query.select(root);
            Query<Employee> employeeQuery = session.createQuery(query);
            return employeeQuery.list();
        }
    }

    //we use join if we need to see also projects
    public Set<Employee> findAllNative(){
        try(Session session = SESSION_FACTORY.openSession()){
            NativeQuery<Object[]> employeeNativeQuery = session.createNativeQuery("SELECT * from employees e,employees_projects es, projects p where  e.employeeid=es.employeeid and es.projectid=p.projectid")
                    .addEntity("e",Employee.class).addJoin("p","e.projects");
            List<Object[]> list = employeeNativeQuery.list();
            Set<Employee> employees = new HashSet<>();
            list.forEach(x -> employees.add((Employee) x[0]));
            return employees;
        }
    }

    public List<Employee> findAllNativeNoJoin(){
        try(Session session = SESSION_FACTORY.openSession()){
            NativeQuery<Employee> employeeNativeQuery = session.createNativeQuery("SELECT * from employees",Employee.class);
            return employeeNativeQuery.list();
        }
    }

    public void findByDepartmentNative(String name){
        try(Session session = SESSION_FACTORY.openSession()) {
            NativeQuery<Employee> nativeQuery = session.createNativeQuery("SELECT * FROM employees e, departments d WHERE e.departmentid = d.departmentid AND d.name = :name").addEntity(Employee.class);
            nativeQuery.setParameter("name",name);
            nativeQuery.list().forEach(e-> System.out.println(e.getEmail()));
        }
    }

    //see number of executed queries if FETCH is removed
    public void findByDepartmentHQL(String name){
        try(Session session = SESSION_FACTORY.openSession()) {
            Query<Employee> query = session.createQuery("SELECT e FROM Employee e LEFT JOIN FETCH e.department WHERE name = :name",Employee.class);
            query.setParameter("name",name);
            query.list().forEach(e-> System.out.println(e.getEmail()));
        }
    }

    //see if you manage to find fetch syntax
    public void findByDepartmentCriteria(String name){
        try(Session session = SESSION_FACTORY.openSession()) {
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Employee> queryBuilder = cb.createQuery(Employee.class);
            Root<Employee> from = queryBuilder.from(Employee.class);
//            queryBuilder.select(from);   -optional,this is default
            Join<Employee, Department> department = from.join("department", JoinType.LEFT);
            queryBuilder.where(cb.equal(department.get("name"),name));
            Query<Employee> query = session.createQuery(queryBuilder);
            query.list().forEach(e-> System.out.println(e.getEmail()));
        }
    }
}
