package com.sda.hibernate.ex1.project;

import com.sda.hibernate.ex1.employee.Employee;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "projects")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "projectId")
    private int id;

    private String description;

    @ManyToMany
    @JoinTable(name = "employees_projects",
    joinColumns = @JoinColumn(name="projectId"),
    inverseJoinColumns = @JoinColumn(name = "employeeId"))
    private List<Employee> employees;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return  description;
    }
}
