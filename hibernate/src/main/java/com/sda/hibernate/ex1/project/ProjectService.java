package com.sda.hibernate.ex1.project;

import com.sda.hibernate.ex1.config.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class ProjectService {
    private static final SessionFactory SESSION_FACTORY = HibernateUtil.getSessionFactory();
    private ProjectRepository projectRepository = new ProjectRepository();

    public void save(Project project){
        try(Session session = SESSION_FACTORY.openSession()){
            session.beginTransaction();
            projectRepository.setSession(session);
            projectRepository.save(project);
            session.getTransaction().commit();
        }
    }

    public void update(int id,String description){
        try(Session session = SESSION_FACTORY.openSession()){
            session.beginTransaction();
            projectRepository.setSession(session);
            Project project = projectRepository.findById(id);
            project.setDescription(description);
            session.getTransaction().commit();
        }
    }

    public void delete(int id){
        try(Session session = SESSION_FACTORY.openSession()){
            session.beginTransaction();
            projectRepository.setSession(session);
            Project project = projectRepository.findById(id);
            if(project!=null) {
                projectRepository.delete(project);
            }
            session.getTransaction().commit();
        }
    }

    public Project findById(int id){
        try(Session session = SESSION_FACTORY.openSession()){
            projectRepository.setSession(session);
            return  projectRepository.findById(id);
        }
    }
}
